export function setProjectData(projectData) {
	if (typeof window == 'undefined') return;
	localStorage.projectData = JSON.stringify({ ...projectData });
}