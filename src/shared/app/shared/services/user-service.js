export function logOut() {
	if (typeof window !== 'undefined')
		localStorage.removeItem('user');
}

export function logIn(user) {
	setUserData(user);
}

export function getUser() {
	if (typeof window == 'undefined') return undefined;
	let user = localStorage.user;
	if (user) {
		return JSON.parse(user);
	}
	return undefined;
}

export function setUserData(user) {
	if (typeof window == 'undefined') return;
	localStorage.user = JSON.stringify({ ...user });
}
