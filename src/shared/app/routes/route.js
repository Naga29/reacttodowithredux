//import Home from "../components/home/home.jsx";
import Login from "../components/log-in/login";
import SignUp from "../components/sign-up/sign-up";
import SuperAdminDash from "../components/super-admin-dash/super-admin-dash.jsx";
export default {
  routes: [
    {
      path: "/",
      component: SignUp,
      exact: true
    },
    {
      path: "/login",
      component: Login,
      exact: true
    },
    {
      path: "/sign-up",
      component: SignUp,
      exact: true
    }
  ],
  redirects: [
    {
      from: "/404",
      to: "/login",
      status: 301
    }
  ],
  private: [
    {
      path: "/admin",
      component: SuperAdminDash,
      exact: false
    }
  ]
};