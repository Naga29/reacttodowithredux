import React, { Component } from "react";
import { Route } from "react-router-dom";
import Projects from "../components/super-admin-dash/projects/projects.jsx";
import AddProjects from "../components/super-admin-dash/projects/new-project/new-project.jsx";
import ViewProject from "../components/super-admin-dash/projects/edit-project/edit-project.jsx";
import ViewToDo from "../components/super-admin-dash/to-dos/to-dos.jsx";
import AddToDo from "../components/super-admin-dash/to-dos/new-todo/new-todo.jsx";
import EditToDo from "../components/super-admin-dash/to-dos/edit-todo/edit-todo.jsx";
const routes = [
  {
    path: "",
    component: Projects
  },
  {
    path: "projects",
    component: Projects
  },
  {
    path: "add-project",
    component: AddProjects
  },
  {
    path: "add-todo",
    component: AddToDo
  },
  {
    path: "to-do",
    component: ViewToDo
  },
  {
    path: "projects/view-project/:project_id",
    component: ViewProject
  },
  {
    path: "to-do/view-todo/:todo_id",
    component: EditToDo
  }
];

class AdminRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {" "}
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })
      } </div>
    );
  }
}

export default AdminRouter;