import { Types } from "../constants/user-types";
const initialState = {
  isLoggedIn: false,
  user:{},
  loginErrorMessage:"",
  redirect:""
};
export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case Types.SAVE_USER:
      return { ...state,isLoggedIn:true, user: action.payload  };
    case Types.REDIRECT:
      return { ...state, redirect: action.payload };
    case Types.LOGIN_MESSAGE:
      return { ...state, loginErrorMessage: action.payload,isLoggedIn:false };
    case Types.LOG_OUT:
      return { ...state, user:initialState.user,isLoggedIn:false };
    default:
      return state;
  }
}