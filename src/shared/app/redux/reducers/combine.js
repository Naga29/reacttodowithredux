import { combineReducers } from 'redux';
import sidebarReducer from "./sidebar-reducer";
import userReducer from "./user-reducer";
import projectReducer from "./project-reducer";
import todoReducer from "./to-do-reducer";

const reducers = combineReducers({
    sidebar:sidebarReducer,
    user:userReducer,
    project:projectReducer,
    todo:todoReducer
});

export default reducers;