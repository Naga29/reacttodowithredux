import { Types } from "../constants/to-do-types";
const initialState = {
  todoList:[],
  toDoStateList:[],
  saveToDoMessage:"",
  saveToDoStatus:true,
  todoData:{}
};
export default function todoReducer(state = initialState, action) {
  switch (action.type) {
    case Types.SAVE_TODO_LIST:
        return { ...state, todoList: action.payload };
    case Types.SAVE_TODO_STATE_LIST:
        return { ...state, toDoStateList: action.payload };
    case Types.SAVE_TODO_MESSAGE:
        return { ...state, saveToDoMessage: action.payload };
    case Types.SAVE_TODO_STATUS:
        return { ...state, saveStatus: action.payload };
    case Types.SAVE_TODO:
        return { ...state, todoData: action.payload };
    default:
      return state;
  }
}