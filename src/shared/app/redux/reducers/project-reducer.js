import { Types } from "../constants/project-types";
const initialState = {
  projectList: [],
  project:{},
  todoList:[],
  toDoStateList:[],
  saveProjectMessage:"",
  saveStatus:true
};
export default function projectReducer(state = initialState, action) {
  switch (action.type) {
    case Types.SAVE_PROJECT_LIST:
      return { ...state,projectList: action.payload  };
    case Types.SAVE_PROJECT:
      return { ...state, project: action.payload };
    case Types.SAVE_TODO_LIST:
        return { ...state, todoList: action.payload };
    case Types.SAVE_TODO_STATE_LIST:
        return { ...state, toDoStateList: action.payload };
    case Types.SAVE_PROJECT_MESSAGE:
        return { ...state, saveProjectMessage: action.payload };
    case Types.SAVE_PROJECT_STATUS:
        return { ...state, saveStatus: action.payload };
    default:
      return state;
  }
}