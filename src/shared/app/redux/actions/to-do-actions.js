import { Types } from "../constants/to-do-types";
import * as ProjectServices from "../../shared/services/project-service";

export function getToDoList() {
  return async function (dispatch, getState) {
    try {
      var projects = JSON.parse(localStorage.projectData);
      var toDo = projects.to_dos;
      var toDoState = projects.to_do_state;
      dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
      dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
    } catch (e) {}
  };
}

export function getToDo(todoId) {
  return async function (dispatch, getState) {
    try {
      var projectData = JSON.parse(localStorage.projectData);
      var todo = projectData.to_dos.find(x=>x.id === todoId);
      if(todo){
        dispatch({ type: Types.SAVE_TODO, payload: todo });
      }
    } catch (e) {}
  };
}


export function AddToDo(todoData) {
    return async function (dispatch, getState) {
      try {
        var projectsData = JSON.parse(localStorage.projectData);
        var projectIndex = projectsData.projects.findIndex(x=>x.id === todoData.project);
        projectsData.projects[projectIndex].to_dos.push(todoData.id)
        var todolist =  projectsData.to_dos;
        todolist.push(todoData);
        ProjectServices.setProjectData(projectsData);
        dispatch({ type: Types.SAVE_TODO_LIST, payload: todolist });
        dispatch({ type: Types.SAVE_TODO_STATUS, payload: true });
        dispatch({ type: Types.SAVE_TODO_MESSAGE, payload: "ToDo saved successfully" });
      } catch (e) {}
    };
  }

  export function updateTODO(tData) {
    return async function (dispatch, getState) {
      try {
        var projectData = JSON.parse(localStorage.projectData);
        var projectIndex = projectData.to_dos.findIndex(x=>x.id === tData.id);
        projectData.to_dos[projectIndex].title = tData.title;
        projectData.to_dos[projectIndex].user_id = tData.user_id;
        projectData.to_dos[projectIndex].project = tData.project;
        projectData.to_dos[projectIndex].description = tData.description;
        projectData.to_dos[projectIndex].state = tData.state;
        projectData.to_dos[projectIndex].due_date = tData.due_date;
        ProjectServices.setProjectData(projectData);
        var toDo = projectData.to_dos;
        var toDoState = projectData.to_do_state;
        dispatch({ type: Types.SAVE_TODO, payload: tData });
        dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
        dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
        dispatch({ type: Types.SAVE_TODO_STATUS, payload: true });
        dispatch({ type: Types.SAVE_TODO_MESSAGE, payload: "ToDo updated successfully" });
      } catch (e) {}
    };
  }

  export function deleteToDo(toDoId) {
    return async function (dispatch, getState) {
      try {
        var projectData = JSON.parse(localStorage.projectData);
        var todoData = projectData.to_dos.find(x=>x.id === toDoId);
        var projectIndex = projectData.projects.findIndex(x=>x.id === todoData.project);
        projectData.projects[projectIndex].to_dos.splice(1,projectData.projects[projectIndex].to_dos.indexOf(toDoId))
        var todoList = projectData.to_dos.filter(x=>x.id !== toDoId);
        projectData.to_dos = todoList;
        ProjectServices.setProjectData(projectData);
        var toDo = projectData.to_dos;
        var toDoState = projectData.to_do_state;
        dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
        dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
        dispatch({ type: Types.SAVE_TODO_STATUS, payload: true });
        dispatch({ type: Types.SAVE_TODO_MESSAGE, payload: "ToDo deleted successfully" });
      } catch (e) {}
    };
  }

  export function updateToDoList(todoList) {
    return async function (dispatch, getState) {
      try {
        var projectData = JSON.parse(localStorage.projectData);
        projectData.to_dos = todoList;
        ProjectServices.setProjectData(projectData);
        var toDo = projectData.to_dos;
        dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
      } catch (e) {}
    };
  }
