import { Types } from "../constants/user-types";
import * as UserServices from "../../shared/services/user-service";

export function login(objUser) {
  return async function(dispatch, getState) {
    try{
          var user = JSON.parse(localStorage.user);
          if(user.userName === objUser.userName && user.password === objUser.password){
            UserServices.logIn(objUser);
            dispatch({ type: Types.REDIRECT, payload: "/admin/projects"});
            dispatch({ type: Types.SAVE_USER, payload: objUser });
          }else{
            dispatch({ type: Types.LOGIN_MESSAGE, payload: "Invalid credentials" });
          }
          
    }catch(e){

    }
    
  };
}

export function SignUp(objUser) {
  return async function(dispatch, getState) {
    try{
        UserServices.setUserData(objUser)
        UserServices.logIn(objUser);
        dispatch({ type: Types.REDIRECT, payload: "/admin"});
        dispatch({ type: Types.SAVE_USER, payload: objUser });
    }catch(e){

    }
    
  };
}

export function getUserData() {
  return async function(dispatch, getState) {
    try{
        var user = JSON.parse(localStorage.user);
        dispatch({ type: Types.SAVE_USER, payload: user });
    }catch(e){

    }
    
  };
}

export function LogOutUser() {
  return async function(dispatch, getState) {
    dispatch({ type: Types.LOG_OUT, payload: {} });
  };
}