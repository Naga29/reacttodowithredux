import { Types } from "../constants/project-types";
import * as ProjectServices from "../../shared/services/project-service";

export function getProjectList() {
  return async function (dispatch, getState) {
    try {
      var projects = JSON.parse(localStorage.projectData);
      var projectList = projects.projects;
      var toDo = projects.to_dos;
      var toDoState = projects.to_do_state;
      dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectList });
      dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
      dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
    } catch (e) {}
  };
}

export function saveProjectList() {
  return async function (dispatch, getState) {
    try {
      if (localStorage.projectData) {
        var projects = JSON.parse(localStorage.projectData);
        var projectList = projects.projects;
        var toDo = projects.to_dos;
        var toDoState = projects.to_do_state;
        dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectList });
        dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
        dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
      } else {
        var projectsData = {
          projects: [
            {
              id: "1111",
              user_id: "5465135",
              name: "Default",
              to_dos: [],
            },
          ],
          to_dos: [],
          to_do_state: [
            { key: "todo", value: "To DO" },
            { key: "inprogress", value: "In Progress" },
            { key: "done", value: "Done" },
          ],
        };
        ProjectServices.setProjectData(projectsData);
        var projectLists = projectsData.projects;
        var toDos = projectsData.to_dos;
        var toDoStates = projectsData.to_do_state;
        dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectLists });
        dispatch({ type: Types.SAVE_TODO_LIST, payload: toDos });
        dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoStates });
      }
    } catch (e) {}
  };
}

export function getProject(projectId) {
  return async function (dispatch, getState) {
    try {
      var projectData = JSON.parse(localStorage.projectData);
      var project = projectData.projects.find((x) => x.id === projectId);
      if (project) {
        dispatch({ type: Types.SAVE_PROJECT, payload: project });
      }
    } catch (e) {}
  };
}

export function AddProject(projectData) {
  return async function (dispatch, getState) {
    try {
      var projectsData = JSON.parse(localStorage.projectData);
      var projectsList = projectsData.projects;
      projectData.to_dos = [];
      projectsList.push(projectData);
      ProjectServices.setProjectData(projectsData);
      dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectsList });
      dispatch({ type: Types.SAVE_PROJECT_STATUS, payload: true });
      dispatch({
        type: Types.SAVE_PROJECT_MESSAGE,
        payload: "Project saved successfully",
      });
    } catch (e) {}
  };
}

export function updateProject(pData) {
  return async function (dispatch, getState) {
    try {
      var projectData = JSON.parse(localStorage.projectData);
      var projectIndex = projectData.projects.findIndex(
        (x) => x.id === pData.id
      );
      projectData.projects[projectIndex].name = pData.name;
      projectData.projects[projectIndex].user_id = pData.user_id;
      projectData.projects[projectIndex].to_dos = pData.to_dos;
      ProjectServices.setProjectData(projectData);
      var projectList = projectData.projects;
      var toDo = projectData.to_dos;
      var toDoState = projectData.to_do_state;
      dispatch({ type: Types.SAVE_PROJECT, payload: pData });
      dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectList });
      dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
      dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
      dispatch({ type: Types.SAVE_PROJECT_STATUS, payload: true });
      dispatch({
        type: Types.SAVE_PROJECT_MESSAGE,
        payload: "Project updated successfully",
      });
    } catch (e) {}
  };
}

export function deleteProject(projectId) {
  return async function (dispatch, getState) {
    try {
      var projectData = JSON.parse(localStorage.projectData);
      var pData = projectData.projects.find((x) => x.id === projectId);
      var todoIds = pData.to_dos;
      var todsLst = [];
      projectData.to_dos.map((todos) => {
        if (todoIds.indexOf(todos.id) < 0) {
          todsLst.push(todos);
        }
        return todsLst;
      });
      projectData.to_dos = todsLst;
      var projects = projectData.projects.filter((x) => x.id !== projectId);
      projectData.projects = projects;
      ProjectServices.setProjectData(projectData);
      var projectList = projectData.projects;
      var toDo = projectData.to_dos;
      var toDoState = projectData.to_do_state;
      dispatch({ type: Types.SAVE_PROJECT_LIST, payload: projectList });
      dispatch({ type: Types.SAVE_TODO_LIST, payload: toDo });
      dispatch({ type: Types.SAVE_TODO_STATE_LIST, payload: toDoState });
      dispatch({ type: Types.SAVE_PROJECT_STATUS, payload: true });
      dispatch({
        type: Types.SAVE_PROJECT_MESSAGE,
        payload: "Project deleted successfully",
      });
    } catch (e) {}
  };
}
