export const Types = {
    SAVE_PROJECT_LIST: "SAVE_PROJECT_LIST",
    SAVE_PROJECT:"SAVE_PROJECT",
    SAVE_TODO_LIST:"SAVE_TODO_LIST",
    SAVE_TODO_STATE_LIST:"SAVE_TODO_STATE_LIST",
    SAVE_PROJECT_MESSAGE:"SAVE_PROJECT_MESSAGE",
    SAVE_PROJECT_STATUS:"SAVE_PROJECT_STATUS"
  };
  