import React from "react";
import {
  Icon
} from "antd";
import {
  ProjectSVG,
  ToDoListSVG
} from "../shared/svg/sidebar/home-icon";

const ProjectIcon = props => < Icon component={
  ProjectSVG
} {
  ...props
  }
/>;
const ToDoIcon = props => < Icon component={
  ToDoListSVG
} {
  ...props
  }
/>;

const adminOptions = [{
  key: "/admin/projects",
  label: "Projects",
  leftIcon: <ProjectIcon />
},
{
  key: "/admin/to-do",
  label: "ToDo",
  leftIcon: <ToDoIcon />
}
];
export default {
  adminOptions
};
