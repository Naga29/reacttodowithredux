import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Input, Button, Select, Row, Col, message, Divider } from "antd";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import * as projectActions from "../../../../redux/actions/project-actions";
const { Option } = Select;
class EditProjectView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: [],
    };
    this.handleBackClick = this.handleBackClick.bind(this);
  }

  handleBackClick(evt) {
    this.props.history.push("/admin/projects");
  }

  async componentDidMount() {
    await this.props.getProject(this.props.match.params.project_id);
    await this.props.getProjectList();
    var todos = this.props.todoList.filter(
      (x) => x.project === this.props.match.params.project_id
    );
    this.setState({ todoList: todos });
  }

  _bindToDos() {
    return this.state.todoList.map((todo, index) => (
      <Option key={"todo" + index} value={todo.id}>
        {todo.title}
      </Option>
    ));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        var project = {
          id: values["id"],
          name: values["name"],
          user_id: values["user_id"],
          to_dos: values["to_dos"],
        };
        await this.props.updateProject(project);
        if (this.props.saveStatus) {
          message.success(this.props.saveProjectMessage);
          this.props.form.resetFields();
        } else message.error(this.props.saveProjectMessage);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>Edit Project</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button
              style={{ float: "right" }}
              onClick={this.handleBackClick}
              className="ant-btn-secondary"
            >
              Back
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <br></br>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Row gutter={24}>
            <Form.Item>
              {getFieldDecorator("id", {
                initialValue: this.props.project.id,
              })(<Input type="hidden" />)}
            </Form.Item>
          </Row>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item label="Name">
                {getFieldDecorator("name", {
                  initialValue: this.props.project.name,
                  rules: [
                    { required: true, message: "Please enter project name!" },
                  ],
                })(<Input placeholder="Name" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item label="User ID">
                {getFieldDecorator("user_id", {
                  initialValue: this.props.project.user_id,
                  rules: [{ required: true, message: "Please enter user id!" }],
                })(<Input placeholder="User ID" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item label="ToDo List">
                {getFieldDecorator("to_dos", {
                  initialValue: this.props.project.to_dos,
                  rules: [{ required: false, message: "Please select todo!" }],
                })(
                  <Select
                    mode="multiple"
                    placeholder="Select todo"
                    style={{ width: 300 }}
                  >
                    {this.state.todoList && this.state.todoList.length > 0
                      ? this._bindToDos()
                      : ""}
                  </Select>
                )}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              style={{ textAlign: "right", paddingRight: "25px" }}
            >
              <Form.Item>
                <Button htmlType="submit" className="ant-btn ant-btn-secondary">
                  Update
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

const WrappedProjectEditView = Form.create({ name: "edit_project" })(
  EditProjectView
);

EditProjectView.propTypes = {
  saveProjectMessage: PropTypes.string,
  saveStatus: PropTypes.bool,
  getProjectList: PropTypes.func,
  todoList: PropTypes.array,
  AddProject: PropTypes.func,
  getProject: PropTypes.func,
  project: PropTypes.object,
  updateProject: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.project,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...projectActions }, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    WrappedProjectEditView
  )
);
