import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../../redux/actions/user-actions";
import * as projectActions from "../../../redux/actions/project-actions";
import { Table, Input, Button, Icon,Row, Col,Divider, message } from "antd";
import Highlighter from "react-highlight-words";
class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectList: [],
    };
    this.handleAddProjectClick = this.handleAddProjectClick.bind(this);
  }

  handleAddProjectClick(evt) {
    this.props.history.push("../admin/add-project");
  }

  handleViewClick = async (key, e) => {
    e.preventDefault();
    this.props.history.push("projects/view-project/" + key);
  };

  handleDeleteClick = async (key, e) => {
    e.preventDefault();
    await this.props.deleteProject(key);
    if (this.props.saveStatus) {
      message.success(this.props.saveProjectMessage);
    } else message.error(this.props.saveProjectMessage);
    var projects = this.props.projectList;
    this.setState({ projectList: projects });
  };
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  async componentDidMount() {
    await this.props.saveProjectList();
    await this.props.getProjectList();
    var projects = this.props.projectList;
    this.setState({ projectList: projects });
  }
  render() {
    const columns = [
      {
        title: "Project Id",
        dataIndex: "id",
        key: "id",
        width: "20%",
        ...this.getColumnSearchProps("id"),
      },
      {
        title: "User Id",
        dataIndex: "user_id",
        key: "user_id",
        width: "20%",
        ...this.getColumnSearchProps("user_id"),
      },
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        width: "20%",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "View",
        dataIndex: "id",
        key: "id ",
        render: (text, project) => (
          <Button
            onClick={(e) => {
              this.handleViewClick(project.id, e);
            }}
            className="ant-btn ant-btn-primary"
          >
            View Detail
          </Button>
        ),
      },
      {
        title: "Delete",
        dataIndex: "id",
        key: "id ",
        render: (text, project) => (
          <Button
            onClick={(e) => {
              this.handleDeleteClick(project.id, e);
            }}
            className="ant-btn ant-btn-primary"
          >
            Delete Project
          </Button>
        ),
      },
    ];

    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>Project List</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button
              style={{ float: "right" }}
              onClick={this.handleAddProjectClick}
              className="ant-btn ant-btn-secondary"
            >
              Add Project
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <Table
          columns={columns}
          rowKey="id"
          dataSource={this.state.projectList}
        />
      </div>
    );
  }
}

Projects.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object,
  projectList: PropTypes.array,
  saveProjectList: PropTypes.func,
  getProjectList: PropTypes.func,
  deleteProject:PropTypes.func
};
function mapStateToProps(state) {
  return {
    ...state.user,
    ...state.project,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...userActions, ...projectActions }, dispatch);
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    Projects
  )
);
