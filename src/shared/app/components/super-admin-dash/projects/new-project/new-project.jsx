import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Input, Button, Select, Row, Col, message,Divider } from "antd";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import * as projectActions from "../../../../redux/actions/project-actions";
const { Option } = Select;
class AddProjectView extends Component {
  constructor(props) {
    super(props);
    this.handleBackClick = this.handleBackClick.bind(this);
  }
  async componentDidMount() {
    await this.props.getProjectList();
  }

  handleBackClick(evt) {
    this.props.history.push("./projects");
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        await this.props.AddProject(values);
        if (this.props.saveStatus) {
          message.success(this.props.saveProjectMessage);
          this.props.form.resetFields();
        } else message.error(this.props.saveProjectMessage);
      }
    });
  };

  _bindToDos() {
    return this.props.todoList.map((todo, index) => (
      <Option key={"todo" + index} value={todo.id}>
        {todo.title}
      </Option>
    ));
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>Add Project</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button style={{ float: "right" }} onClick={this.handleBackClick} className="ant-btn-secondary">
              Back
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <br></br>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("id", {
                  rules: [
                    { required: true, message: "Please enter id!" }
                  ]
                })(<Input placeholder="Id" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("name", {
                  rules: [
                    { required: true, message: "Please enter name!" }
                  ]
                })(<Input placeholder="Name" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("user_id", {
                  rules: [
                    { required: true, message: "Please enter user_id!" }
                  ]
                })(<Input placeholder="User Id" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              style={{ textAlign: "right", paddingRight: "25px" }}
            >
              <Form.Item>
                <Button htmlType="submit" className="ant-btn-primary">Add Project</Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

const WrappedProjectAddView = Form.create({ name: "add_project" })(AddProjectView);

AddProjectView.propTypes = {
  saveProjectMessage: PropTypes.string,
  saveStatus: PropTypes.bool,
  getProjectList:PropTypes.func,
  todoList:PropTypes.array,
  AddProject:PropTypes.func
};

function mapStateToProps(state) {
  return {
    ...state.project
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...projectActions }, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    WrappedProjectAddView
  )
);
