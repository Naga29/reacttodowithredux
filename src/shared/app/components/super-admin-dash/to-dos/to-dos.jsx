import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../../redux/actions/user-actions";
import * as projectActions from "../../../redux/actions/project-actions";
import * as todoActions from "../../../redux/actions/to-do-actions";
import { Table, Input, Button, Icon, Row, Col, Divider, message } from "antd";
import Highlighter from "react-highlight-words";
import {
  sortableContainer,
  sortableElement,
  sortableHandle,
} from "react-sortable-hoc";
import { MenuOutlined } from "@ant-design/icons";
import arrayMove from "array-move";
import Moment from "react-moment";
const DragHandle = sortableHandle(() => (
  <MenuOutlined style={{ cursor: "pointer", color: "#999" }} />
));

class ToDos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: [],
    };
    this.handleAddProjectClick = this.handleAddProjectClick.bind(this);
  }

  setClassName(todo){
    var tempClassName = "";
    var now = new Date();
    var dateCompare = new Date(todo.due_date);
    if (dateCompare < now) {
      tempClassName = "due-date";
    }
    return tempClassName;
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { todoList } = this.state;
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(todoList), oldIndex, newIndex).filter(
        (el) => !!el
      );
      this.props.updateToDoList(newData);
      this.setState({ todoList: newData });
    }
  };

  handleAddProjectClick(evt) {
    this.props.history.push("../admin/add-todo");
  }

  handleViewClick = async (key, e) => {
    e.preventDefault();
    this.props.history.push("to-do/view-todo/" + key);
  };

  handleDeleteClick = async (key, e) => {
    e.preventDefault();
    await this.props.deleteToDo(key);
    if (this.props.saveToDoStatus) {
      message.success(this.props.saveToDoMessage);
    } else message.error(this.props.saveToDoMessage);
    this.setState({ todoList: this.props.todoList });
  };
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };
  async componentDidMount() {
    await this.props.getToDoList();
    var todos = this.props.todoList;
    this.setState({ todoList: todos });
  }
  render() {
    const columns = [
      {
        title: "Sort",
        dataIndex: "sort",
        width: "7%",
        className: "drag-visible",
        render: () => <DragHandle />,
      },
      {
        title: "Todo Id",
        dataIndex: "id",
        key: "id",
        width: "10%",
        className: "drag-visible",
        ...this.getColumnSearchProps("id"),
      },
      {
        title: "Project",
        dataIndex: "project",
        key: "project",
        width: "20%",
        ...this.getColumnSearchProps("project"),
      },
      {
        title: "Description",
        dataIndex: "description",
        key: "description",
        width: "20%",
        ...this.getColumnSearchProps("description"),
      },
      {
        title: "Title",
        dataIndex: "title",
        key: "title",
        width: "20%",
        ...this.getColumnSearchProps("title"),
      },
      {
        title: "Due Date",
        dataIndex: "due_date",
        key: "due_date",
        width: "20%",
        className: "",
        render: (text, todo) => (
          <span className={this.setClassName(todo)}>
            <Moment format={"DD/MM/YYYY"}>{todo.due_date}</Moment>
          </span>
        ),
      },
      {
        title: "View",
        dataIndex: "id",
        key: "id ",
        render: (text, project) => (
          <Button
            onClick={(e) => {
              this.handleViewClick(project.id, e);
            }}
            className="ant-btn ant-btn-primary"
          >
            View Detail
          </Button>
        ),
      },
      {
        title: "Delete",
        dataIndex: "id",
        key: "id ",
        render: (text, project) => (
          <Button
            onClick={(e) => {
              this.handleDeleteClick(project.id, e);
            }}
            className="ant-btn ant-btn-primary"
          >
            Delete ToDo
          </Button>
        ),
      },
    ];

    const DraggableContainer = (props) => (
      <SortableContainer
        useDragHandle
        helperClass="row-dragging"
        onSortEnd={this.onSortEnd}
        {...props}
      />
    );

    const SortableItem = sortableElement((props) => <tr {...props} />);
    const SortableContainer = sortableContainer((props) => (
      <tbody {...props} />
    ));
    const DragableBodyRow = ({ index, className, style, ...restProps }) => (
      <SortableItem index={restProps["data-row-key"]} {...restProps} />
    );
    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>ToDo List</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button
              style={{ float: "right" }}
              onClick={this.handleAddProjectClick}
              className="ant-btn ant-btn-secondary"
            >
              Add ToDo
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <Table
          pagination={true}
          dataSource={this.state.todoList}
          columns={columns}
          rowKey="index"
          components={{
            body: {
              wrapper: DraggableContainer,
              row: DragableBodyRow,
            },
          }}
        />
      </div>
    );
  }
}

ToDos.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
  match: PropTypes.object,
  projectList: PropTypes.array,
  getProjectList: PropTypes.func,
  saveProjectMessage: PropTypes.string,
  saveStatus: PropTypes.bool,
  updateToDoList: PropTypes.func,
  deleteToDo: PropTypes.func,
};
function mapStateToProps(state) {
  return {
    ...state.user,
    ...state.project,
    ...state.todo,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { ...userActions, ...projectActions, ...todoActions },
    dispatch
  );
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    ToDos
  )
);
