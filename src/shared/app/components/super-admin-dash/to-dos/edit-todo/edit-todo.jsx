import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Input, Button, Select, Row, Col, message, Divider, DatePicker } from "antd";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import * as projectActions from "../../../../redux/actions/project-actions";
import * as todoActions from "../../../../redux/actions/to-do-actions";
import moment from 'moment';
const { Option } = Select;
class EditToDoView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todoList: [],
    };
    this.handleBackClick = this.handleBackClick.bind(this);
  }

  handleBackClick(evt) {
    this.props.history.push("/admin/to-do");
  }

  async componentDidMount() {
    await this.props.getToDo(this.props.match.params.todo_id);
    await this.props.getProjectList();
  }

  _bindProjects() {
    return this.props.projectList.map((todo, index) => (
      <Option key={"todo" + index} value={todo.id}>
        {todo.name}
      </Option>
    ));
  }

  _bindState() {
    return this.props.toDoStateList.map((state, index) => (
      <Option key={"todo" + index} value={state.key}>
        {state.value}
      </Option>
    ));
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        var todo = {
          id: values["id"],
          title: values["title"],
          user_id: values["user_id"],
          project: values["project"],
          state: values["state"],
          description: values["description"],
          due_date: values["due_date"],
        };
        await this.props.updateTODO(todo);
        if (this.props.saveToDoStatus) {
          message.success(this.props.saveToDoMessage);
        } else message.error(this.props.saveToDoMessage);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>Edit ToDo</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button
              style={{ float: "right" }}
              onClick={this.handleBackClick}
              className="ant-btn-secondary"
            >
              Back
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <br></br>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("id", {
                  initialValue: this.props.todoData.id,
                  rules: [{ required: true, message: "Please enter id!" }],
                })(<Input disabled placeholder="Id" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("title", {
                  initialValue: this.props.todoData.title,
                  rules: [{ required: true, message: "Please enter title!" }],
                })(<Input placeholder="Title" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("user_id", {
                  initialValue: this.props.todoData.user_id,
                  rules: [{ required: true, message: "Please enter user_id!" }],
                })(<Input placeholder="User Id" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("description", {
                  initialValue: this.props.todoData.description,
                  rules: [
                    { required: true, message: "Please enter description!" },
                  ],
                })(<Input placeholder="Description" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item label="Project">
                {getFieldDecorator("project", {
                  initialValue: this.props.todoData.project,
                  rules: [
                    { required: false, message: "Please select project!" },
                  ],
                })(
                  <Select placeholder="Select project" style={{ width: 300 }}>
                    {this._bindProjects()}
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item label="State">
                {getFieldDecorator("state", {
                  initialValue: this.props.todoData.state,
                  rules: [{ required: false, message: "Please select state!" }],
                })(
                  <Select placeholder="Select State" style={{ width: 300 }}>
                    {this._bindState()}
                  </Select>
                )}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("due_date", {
                  initialValue: moment(this.props.todoData.due_date),
                  rules: [
                    { required: true, message: "Please select due date!" }
                  ]
                })(<DatePicker />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              style={{ textAlign: "right", paddingRight: "25px" }}
            >
              <Form.Item>
                <Button htmlType="submit" className="ant-btn ant-btn-secondary">
                  Update
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

const WrappedToDoEditView = Form.create({ name: "edit_project" })(EditToDoView);

EditToDoView.propTypes = {
  saveToDoMessage: PropTypes.string,
  saveToDoStatus: PropTypes.bool,
  getProjectList: PropTypes.func,
  todoList: PropTypes.array,
  addToDo: PropTypes.func,
  todoData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    ...state.project,
    ...state.todo,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...projectActions, ...todoActions }, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    WrappedToDoEditView
  )
);
