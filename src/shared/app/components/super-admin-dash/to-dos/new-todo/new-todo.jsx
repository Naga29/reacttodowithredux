import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Form, Input, Button, Select, Row, Col, message,Divider, DatePicker } from "antd";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux"; 
import * as projectActions from "../../../../redux/actions/project-actions";
import * as todoActions from "../../../../redux/actions/to-do-actions";
const { Option } = Select;
class AddToDOView extends Component {
  constructor(props) {
    super(props);
    this.handleBackClick = this.handleBackClick.bind(this);
  }
  async componentDidMount() {
    await this.props.getProjectList();
  }

  handleBackClick(evt) {
    this.props.history.push("./to-do");
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        await this.props.AddToDo(values);
        if (this.props.saveToDoStatus) {
          message.success("ToDo Added successfully!");
          this.props.form.resetFields();
        } else message.error(this.props.saveToDoMessage);
      }
    });
  };

  _bindProjects() {
    return this.props.projectList.map((todo, index) => (
      <Option key={"todo" + index} value={todo.id}>
        {todo.name}
      </Option>
    ));
  }

  _bindState() {
    return this.props.toDoStateList.map((state, index) => (
      <Option key={"todo" + index} value={state.key}>
        {state.value}
      </Option>
    ));
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Row gutter={24}>
          <Col xs={12} sm={12} md={16} lg={16} xl={16}>
            <h2>Add ToDo</h2>
          </Col>
          <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Button style={{ float: "right" }} onClick={this.handleBackClick} className="ant-btn-secondary">
              Back
            </Button>
          </Col>
        </Row>
        <Divider />
        <br></br>
        <br></br>
        <br></br>
        <Form layout="inline" onSubmit={this.handleSubmit}>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("id", {
                  rules: [
                    { required: true, message: "Please enter id!" }
                  ]
                })(<Input placeholder="Id" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("title", {
                  rules: [
                    { required: true, message: "Please enter title!" }
                  ]
                })(<Input placeholder="Title" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("user_id", {
                  rules: [
                    { required: true, message: "Please enter user_id!" }
                  ]
                })(<Input placeholder="User Id" />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("description", {
                  rules: [
                    { required: true, message: "Please enter description!" }
                  ]
                })(<Input placeholder="Description" />)}
              </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Form.Item label="Project">
              {getFieldDecorator("project", {
                initialValue: this.props.project.project,
                rules: [{ required: false, message: "Please select project!" }]
              })(<Select placeholder="Select project" style={{ width: 300 }}>
                {this._bindProjects()}
              </Select>)}
            </Form.Item>
            </Col>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
            <Form.Item label="State">
              {getFieldDecorator("state", {
                initialValue: this.props.project.state,
                rules: [{ required: false, message: "Please select state!" }]
              })(<Select placeholder="Select State" style={{ width: 300 }}>
                {this._bindState()}
              </Select>)}
            </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={12} sm={12} md={8} lg={8} xl={8}>
              <Form.Item>
                {getFieldDecorator("due_date", {
                  rules: [
                    { required: true, message: "Please select due date!" }
                  ]
                })(<DatePicker />)}
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col
              xs={24}
              sm={24}
              md={24}
              lg={24}
              xl={24}
              style={{ textAlign: "right", paddingRight: "25px" }}
            >
              <Form.Item>
                <Button htmlType="submit" className="ant-btn-primary">Add ToDo</Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

const WrappedToDoAddView = Form.create({ name: "add_project" })(AddToDOView);

AddToDOView.propTypes = {
  saveToDoMessage: PropTypes.string,
  saveToDoStatus: PropTypes.bool,
  getProjectList:PropTypes.func,
  todoList:PropTypes.array,
  AddToDo:PropTypes.func
};

function mapStateToProps(state) {
  return {
    ...state.project,
    ...state.todo
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...projectActions, ...todoActions }, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(
    WrappedToDoAddView
  )
);
