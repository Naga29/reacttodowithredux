import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Icon, Row, Col, Menu, Dropdown } from "antd";
import * as sidebarAction from "../../../redux/actions/sidebar-actions";
import * as userActions from "../../../redux/actions/user-actions"
class AdminHeader extends Component {
  constructor(props) {
    super(props);
    this.state = { collapsed: true };
    this.logout = this.logout.bind(this);
  }
  profile = () => (
    <Menu style={{ width: 200 }}>
      <Menu.Item key="0" onClick={this.logout}>
        <span>Log Out</span>
      </Menu.Item>
    </Menu>
  );

  
  toggle = () => {
    this.props.toggleSidebar();
  };
  async logout() {
    await this.props.LogOutUser();
    this.props.history.push("/login");
  }
  componentDidMount() {
    //console.log(this.state.collapsed);
  }

  render() {
    return (
      <div>
        <Row gutter={24}>
          <Col xs={3}>
            <Icon
              className="trigger"
              type={!this.props.sidebar ? "menu-unfold" : "menu-fold"}
              onClick={this.toggle}
            />
          </Col>
          <Col xs={21}>
            <ul>
              <li>
                <Dropdown overlay={this.profile} trigger={["click"]}>
                  <Link className="ant-dropdown-link" to="">
                    <i className="fas fa-user-circle"></i>
                    <span className="name">
                      {this.props.user.userName}
                    </span>
                  </Link>
                </Dropdown>
              </li>
            </ul>
          </Col>
        </Row>
      </div>
    );
  }
}

AdminHeader.propTypes = {
  toggleSidebar: PropTypes.func,
  LogOutUser:PropTypes.func,
  user:PropTypes.object
};

function mapStateToProps(state) {
  return {
    ...state.sidebar,
    ...state.user
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...sidebarAction,...userActions}, dispatch);
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AdminHeader)
);
