import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Form, Icon, Input, Button } from 'antd';
import 'antd/dist/antd.css';
var passwordValidator = require('password-validator');
var schema = new passwordValidator();
schema.is().min(8)                                    // Minimum length 8
schema.is().max(100)                                  // Maximum length 100
schema.has().uppercase()                              // Must have uppercase letters
schema.has().lowercase()                              // Must have lowercase letters
schema.has().digits()                                 // Must have digits
schema.has().not().spaces();                           // Should not have spaces
class NormalSignUpForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      errorMessage:""
    }
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    var self = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var userObject = {
          "userName":values.username,
          "password":values.password,
          "email":values.email
        }
        if(schema.validate(values.password) === false){
          this.setState({errorMessage:"Enter a strong password (Conatins min 8 character, 1 uppercase, 1 dighit without spaces!"})
        }else{
          self.props.signUp(userObject);
        }
        
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <h3>Sign Up</h3>
        <Form.Item label="Username">
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />,
          )}
        </Form.Item>
        <Form.Item label="Email">
          {getFieldDecorator('email', {
            rules: [{ required: false, message: 'Please input your email!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Email (Optional)"
            />,
          )}
        </Form.Item>
        <Form.Item label="Password">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
        <Form.Item className="form-btm">
          <Button type="primary" htmlType="submit" className="login-form-button">
            Sign Up
          </Button>
          <span className="error-message">{this.props.loginErrorMessage || this.state.errorMessage}</span>
          <br></br>
          <Link type="primary" to="login" htmlType="submit" className="login-form-button">
            Log in
          </Link>
        </Form.Item>
      </Form>
    );
  }
}

export const WrappedNormalSignUpForm = Form.create({ name: 'normal_signup' })(NormalSignUpForm);
