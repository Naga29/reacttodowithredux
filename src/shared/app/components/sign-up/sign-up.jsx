import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {WrappedNormalSignUpForm} from "./sign-up-from/sign-up-form.jsx";
import * as userActions from "../../redux/actions/user-actions"
class SignUp extends Component {
  constructor(props){
    super(props);
    this.signUp=this.signUp.bind(this);
  }
  async signUp(objUser){
    await this.props.SignUp(objUser);
    if(this.props.isLoggedIn){
      this.props.history.push(this.props.redirect)
    }
  }
  render() {
    return (
      <div className="form-wraper">
        <WrappedNormalSignUpForm signUp={this.signUp} history={this.props.history} location={this.props.location}/>
      </div>
    );
  }
}

SignUp.propTypes = {
  SignUp: PropTypes.func,
  user:PropTypes.object,
  redirect:PropTypes.string,
  history:PropTypes.object,
  location:PropTypes.object,
  isLoggedIn:PropTypes.bool
};
function mapStateToProps(state) {
  return {
    ...state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
     ...userActions,
  }, dispatch);
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(SignUp));

