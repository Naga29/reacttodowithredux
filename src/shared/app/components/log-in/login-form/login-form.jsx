import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Form, Icon, Input, Button } from 'antd';
import 'antd/dist/antd.css';

class NormalLoginForm extends Component {
  handleSubmit = async (e) => {
    e.preventDefault();
    var self = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        var userObject = {
          "userName":values.username,
          "password":values.password
        }
        self.props.signIn(userObject);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <h3>Sign In</h3>
        <Form.Item label="Username">
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Username"
            />,
          )}
        </Form.Item>
        <Form.Item label="Password">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />,
          )}
        </Form.Item>
        <Form.Item className="form-btm">
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
          </Button>
          <span className="error-message">{this.props.loginErrorMessage}</span>
          <br></br>
          <Link type="primary" to="sign-up" htmlType="submit" className="login-form-button">
            Sign Up
          </Link>
        </Form.Item>
      </Form>
    );
  }
}

export const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(NormalLoginForm);
