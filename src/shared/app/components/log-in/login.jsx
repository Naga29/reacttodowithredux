import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import {WrappedNormalLoginForm} from "./login-form/login-form.jsx";
import * as userActions from "../../redux/actions/user-actions"
class Login extends Component {
  constructor(props){
    super(props);
    this.signIn=this.signIn.bind(this);
  }
  async signIn(objUser){
    await this.props.login(objUser);
    if(this.props.isLoggedIn){
      this.props.history.push(this.props.redirect)
    }
  }
  render() {
    return (
      <div className="form-wraper">
        <WrappedNormalLoginForm signIn={this.signIn} loginErrorMessage={this.props.loginErrorMessage} history={this.props.history} location={this.props.location}/>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func,
  loginErrorMessage:PropTypes.string,
  user:PropTypes.object,
  redirect:PropTypes.string,
  history:PropTypes.object,
  location:PropTypes.object,
  isLoggedIn:PropTypes.bool
};
function mapStateToProps(state) {
  return {
    ...state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
     ...userActions,
  }, dispatch);
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(Login));

